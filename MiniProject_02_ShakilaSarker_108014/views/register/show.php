<?php
include_once("../../" . "vendor/autoload.php");
use \App\Registration\Registration;
use \App\Utility\Utility;

$obj = new Registration();
$var = $obj->show($_GET['id']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
        <title>Registration List</title>
    </head>
   
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                       <h2 style="color : #245269">Registration List</h2> 
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Birthday</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Country</th>
                                <th>Gender</th>
                                <th>Term</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td><?php echo $var->name; ?></td>
                                     <td><?php echo $var->birthday; ?></td>
                                    <td><?php echo $var->email; ?></td>
                                    <td><?php echo $var->mobile; ?></td>
                                    <td><?php echo $var->country; ?></td>
                                    <td><?php echo $var->gender; ?></td>
                                   <td><?php echo $var->term; ?></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <a href="create.php"><b>REGISTRATION</b></a>
                </div>
            </div>
        </div>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../resource/js/bootstrap.min.js"></script>
    </body>
</html>