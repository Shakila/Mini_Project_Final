
<html>
       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
              <title>Registration</title>
       </head>

       <body>
              <div class="container">
                     <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                   <h3>Registration</h3>
                            </div>
                     </div>
                     <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                   <form role="form" action="store.php" method="post">
                                          <div class="form-group">
                                                 <label for="name">Name:</label>
                                                 <input type="text" name="name" class="form-control" id="name">
                                          </div>
                                          <div class="form-group">
                                                 <label for="birthday">Birthday:</label>
                                                 <input type="date" name="birthday" class="form-control" id="birthday">
                                          </div>
                                          <div class="form-group">
                                                 <label for="email">Email:</label>
                                                 <input type="email" name="email" class="form-control" id="email">
                                          </div>
                                          <div class="form-group">
                                                 <label for="mobile">Mobile#</label>
                                                 <input type="text" name="mobile" pattern="[+]{1}[0-9]{13}" class="form-control" id="mobile" placeholder="+880**********">
                                          </div>
                                          <div class="form-group">
                                                 <label class="control-label col-sm-2" for="country">Country:</label>
                                                 <div class="col-sm-10">          
                                                        <select class="form-control" name="country" id="country" >
                                                               <option value='Bangladesh'>Bangladesh</option>
                                                               <option value='Spain'>Spain</option>
                                                               <option value='Dubai'>Dubai</option>
                                                               <option value='Canada'>Canada</option>
                                                        </select>
                                                 </div>
                                          </div>
                                          <div class="form-group">  
                                                 <label class="col-sm-2" for="gender">Gender:</label>
                                                 <div class="radio col-sm-10">
                                                        <label><input type="radio" name="gender" value="Male">Male</label>
                                                        <label><input type="radio" name="gender" value="Female">Female</label>
                                                 </div>
                                          </div>
                                          <div class="form-group">  
                                                 <label class="col-sm-2" for="term">Term:</label>
                                                 <div class="radio col-sm-10">
                                                        <label><input type="radio" name="term" value="Agree">Agree</label>
                                                        <label><input type="radio" name="term" value="Not Agree">Not Agree</label>
                                                 </div>
                                          </div>    

                                          <div class="btn-group">
                                                 <button type="submit" class="btn btn-info">Submit</button>
                                                 <button type="submit" class="btn btn-warning"><a href="index.php">List</a></button>
                                          </div>
                                   </form>
                            </div>
                     </div>
              </div>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
              <script src="../../resource/js/bootstrap.min.js"></script>
       </body>
</html>