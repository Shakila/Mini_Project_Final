<?php
include_once("../../"."vendor/autoload.php");

use \App\Mini_Project\Student\Std;
use \App\Mini_Project\Utility\Utility;

$obj = new Std();
$view = $obj->show($_GET['id']);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Phone Book Information List</title>
        <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        h2{
            color : rosybrown;
        }
        button .btn-info{
            color : white;
        }
    </style>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h2>Student Information View</h2> 
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Student ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Major</th>
                                <th>Gender</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td><?php echo $view->student_id; ?></td>
                                    <td><?php echo $view->name; ?></td>
                                    <td><?php echo $view->email; ?></td>
                                    <td><?php echo $view->major; ?></td>
                                    <td><?php echo $view->gender; ?></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <a href="index.php"><b>Go back to List</b></a>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
