<?php
include_once("../../"."vendor/autoload.php");

use \App\Mini_Project\Student\Std;
use \App\Mini_Project\Utility\Utility;

$obj = new Std();
$var = $obj->index();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
        <title>Information List</title>
    </head>
   
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                       <h2 style="color : #245269">Registration List</h2> 
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Student ID</th>
                                <th>Name</th>
                                <th>Birth Date</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Major</th>
                                <th>Action</th>
                                <th>Action</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                              <?php
                            foreach ($var as $reg) {
                                ?>
                                <tr>
                                    <td><?php echo $reg['student_id'] ?></td>
                                    <td><?php echo $reg['name'] ?></td>
                                     <td><?php echo $reg['birth_date'] ?></td>
                                    <td><?php echo $reg['email'] ?></td>
                                    <td><?php echo $reg['gender'] ?></td>
                                    <td><?php echo $reg['major'] ?></td>
                                    <td><button type="submit" class="btn btn-warning"><a href="edit.php?id=<?php echo $reg['id']; ?>">Update</a></button> </td>
                                    <td><button type="submit" class="btn btn-info"><a href="show.php?id=<?php echo $reg['id'];?>">View</a></button> </td>
                                    <td>    
                                        <form action="delete.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $reg['id']; ?>">
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <a href="create.php"><b>Entry | </b></a>
                    <a href="simple-download-xlsx.php"><b> XLSX |</b></a>
                    <a href="pdf.php"><b>  PDF</b></a>
                </div>
            </div>
        </div>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../resource/js/bootstrap.min.js"></script>
    </body>
</html>