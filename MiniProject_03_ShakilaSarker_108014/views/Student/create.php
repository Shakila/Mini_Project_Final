
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Insert Student Information</title>
        <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        h3{
            color : rosybrown;
        }
    </style>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h3>Student Information</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form role="form"  action="store.php" method="post">
                        <input type="hidden" name="id"  />
                        <div class="form-group">
                            <label for="student_id">Student Id:</label>
                            <input type="text" name="student_id" class="form-control" id="student_id">
                        </div>
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" name="name" class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="birth_date">Birth date:</label>
                            <input type="date" name="birth_date" class="form-control" id="birth_date">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" name="email" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender:</label>
                            <div class="radio">
                                <label><input type="radio" name="gender" value="Male">Male</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="gender" value="Female">Female</label>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="major">Major:</label>
                            <div class="radio">
                                <label><input type="radio" name="major" value="Science">Science</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="major" value="Arts">Arts</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="major" value="Commerce">Commerce</label>
                            </div>
                        </div>
                       <div class="btn-group">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="submit" class="btn btn-warning"><a href="index.php">List</a></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../resource/js/bootstrap.min.js"></script>
    </body>
</html>