<?php
ini_set("display_errors","On");
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."MiniProject_03_ShakilaSarker_108014".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");


use \App\Mini_Project\Student\Std;
use \App\Mini_Project\Utility\Utility;

$obj = new Std();
$books = $obj->index();
$trs = "";

?>


                
                <?php
                $slno =0;
                foreach($books as $book):
                    $slno++;
                    $trs .="<tr>";
                    $trs .="<td>".$slno."</td>";
                    $trs .="<td>".$book['student_id']."</td>";
                    $trs .="<td>".$book['name']."</td>";
                    $trs .="<td>".$book['birth_date']."</td>";
                    $trs .="<td>".$book['email']."</td>";
                    $trs .="<td>".$book['gender']."</td>";
                    $trs .="<td>".$book['major']."</td>";
                    $trs .="</tr>";
                 endforeach;   
                ?>


<?php

$html = <<<BITM
<!DOCTYPE html>
<html>
    <head>
        <title>List of Books</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        </style>
    
    </head>
    
    <body>
        <h1>Informatioion</h1>
     
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    
                    <th>Student ID &dArr;</th>
                    
                    <th>Name &dArr;</th>
                    <th>Birthday &dArr;</th>
                         <th>Email &dArr;</th>
                          
        <th>Gender &dArr;</th>
        
        <th>Major &dArr;</th>
                     
                
                </tr>
            </thead>
            <tbody>
        
              echo $trs;
        
        
        
        </tbody>
        </table>
       
        
            

    </body>
</html>
BITM;
?>
<?php

require_once $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."MiniProject_03_ShakilaSarker_108014".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf'.DIRECTORY_SEPARATOR.'mpdf.php';

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

