<?php

namespace App\Mini_Project\PhoneBook_class;

use \App\Mini_Project\Utility\Utility;

class Phone_book {

    public $id = "";
    public $name = "";
    public $email = "";
    public $mobile = "";
    public $address = "";
    public $deleted_at = NULL; //soft delete
    
    public function __construct() {
        $conn = mysql_connect("localhost", "root", "") or die("Can not connect Database");
        $lnk = mysql_select_db("mini_project_1") or die("can not connect table");
    }

    public function trashed() {
        $_info = array();

        $query = "SELECT * FROM `phone_book` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_info[] = $row;
        }

        return $_info;
        //Utility::redirect('trashed.php');
    }
    
     public function index() {
        $_info = array();

        $query = "SELECT * FROM `phone_book` WHERE deleted_at IS NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $_info[] = $row;
        }

        return $_info;
        //Utility::redirect('index.php');
    }

    public function store($data = array()) {
        $query = "INSERT INTO `phone_book` (`name`,  `email` ,`mobile`,  `address`) VALUES ('" . $this->name . "', '" . $this->email . "', '" . $this->mobile . "', '" . $this->address . "')";

        if (mysql_query($query)) {
            // Message::set('Book data is added successfully.');
        } else {
            // Message::set('There is an error while storing book information, please try again.');
        }


        Utility::redirect('index.php');
    }

    public function show($id = null){
        
        if(is_null($id)){
            return;
        }
        
        $query = "SELECT * FROM `phone_book` WHERE id = ".$id;
       
        $result = mysql_query($query); 
        $phnbook = mysql_fetch_object($result);
        
        return $phnbook;
        
    }

    public function edit($id = null) {

        if (is_null($id)) {
            return;
        }

        $query = "SELECT * FROM `phone_book` WHERE id = " . $id;

        $result = mysql_query($query);
        $phoneBook = mysql_fetch_object($result);

        return $phoneBook;
    }

    public function update() {

        $query = "UPDATE `phone_book` SET `name` = '" . $this->name . "', `email` = '" . $this->email . "', `mobile` = '" . $this->mobile . "', `address` = '" . $this->address . "' WHERE `id` = " . $this->id;
        //var_dump($query);
        //die();
        if (mysql_query($query)) {
            //Message::set('Book data is updated successfully.');
        } else {
            //Message::set('There is an error while storing book information, please try again.');
        }


        Utility::redirect('index.php');
    }

        public function delete($id = null){
        
        if(is_null($id)){
            return;
        }
        
        $query = "DELETE FROM `phone_book` WHERE `id`=".$id;
       
        if( mysql_query($query)){
           // Message::set('Book data is deleted successfully.');
        }else{
            //Message::set('There is an error while deleting book information, please try again.');
        }
        
        
        Utility::redirect('index.php');
        
    }
  public function trash($id = null){
        
        if(is_null($id)){
            return;
        }
        $this->id = $id;
        $this->deleted_at = time();
        
        $query = "UPDATE `phone_book` SET `deleted_at` = '" .$this->deleted_at . "' WHERE `id` = " . $this->id;
       
        if( mysql_query($query)){
           // Message::set('Book data is deleted successfully.');
        }else{
            //Message::set('There is an error while deleting book information, please try again.');
        }
        
        
        Utility::redirect('index.php');
        
    }
    
    public function recover($id = null){
        
        if(is_null($id)){
            return;
        }
        $this->id = $id;
        
        $query = "UPDATE `phone_book` SET `deleted_at` =  NULL  WHERE `id` = " . $this->id;
       
        if( mysql_query($query)){
           // Message::set('Book data is deleted successfully.');
        }else{
            //Message::set('There is an error while deleting book information, please try again.');
        }
        
        
        Utility::redirect('index.php');
        
    }
    
    public function prepare($data = array()) {
        if (is_array($data) && array_key_exists('name', $data)) {
            $this->name = $data['name'];
            $this->email = $data['email'];
            $this->mobile = $data['mobile'];
            $this->address = $data['address'];
            $this->deleted_at = $data['deleted_at'];

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
        }
        return $this;
    }

}

?>