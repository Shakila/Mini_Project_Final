-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2016 at 04:16 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mini_project_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `phone_book`
--

CREATE TABLE IF NOT EXISTS `phone_book` (
`id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phone_book`
--

INSERT INTO `phone_book` (`id`, `name`, `email`, `mobile`, `address`, `deleted_at`) VALUES
(1, 'Shakila Sarker', 'keya.sakila@yahoo.com', '+8801879076980', 'Dhaka', '1454296415'),
(3, 'Orpa', 'orpa@gmail.com', '+8801879976980', 'Gazipur', NULL),
(4, 'Kakon', 'kakon@gmail.com', '+9901876890176', 'Dubai', NULL),
(5, 'Rimon Ahmed', 'ri.mon@yahoo.com', '+9901876890176', 'Tejgao', NULL),
(6, 'Seline Begum', 'seline_begum@gmail.com', '+8801689098298', 'Dhaka', NULL),
(10, 'Bony Chy', 'k@yahoo.com', '+8803257498564', 'gfdg', NULL),
(11, 'Bony Chy', 'k@yahoo.com', '+8803257498564', 'gfdg', '1454296429'),
(12, 'Sima Chy', 'k@yahoo.com', '+8803257498564', 'gfdg', NULL),
(13, 'Bony Chy', 'k@yahoo.com', '+8803257498564', 'gfdg', '1454296427'),
(15, 'rocky', 'abrocky39@yahoo.com', '+8801966123482', 'lalbag', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phone_book`
--
ALTER TABLE `phone_book`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phone_book`
--
ALTER TABLE `phone_book`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
