<?php 
include_once("vendor/autoload.php");
use \App\Mini_Project\Utility\Utility;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link href="resource/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
          
                    <?php  Utility::redirect('views/PhoneBook/index.php'); ?>
               
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="resource/js/bootstrap.min.js"></script>
    </body>
</html>