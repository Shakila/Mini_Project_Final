<?php
session_start();
include_once("../../" . "vendor/autoload.php");


use \App\Mini_Project\PhoneBook_class\Phone_book;
use \App\Mini_Project\Utility\Utility;

$obj = new Phone_book();
$var = $obj->trashed();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Phone Book Information List</title>
        <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        h2{
            color : rosybrown;
        }
    </style>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h2>Trash List</h2> 
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Address</th>
                                <th>Action</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($var as $pbook) {
                                ?>
                                <tr>
                                    <td><?php echo $pbook['name'] ?></td>
                                    <td><?php echo $pbook['email'] ?></td>
                                    <td><?php echo $pbook['mobile'] ?></td>
                                    <td><?php echo $pbook['address'] ?></td>
                                    <td><button type="submit" class="btn btn-info"><a href="recover.php?id=<?php echo $pbook['id'];?>">Recover</a></button> </td>
                                    <td>    
                                        <form action="delete.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $pbook['id']; ?>">
                                            <button type="submit" class="btn btn-danger">Permanent Delete</button>
                                        </form>
                                    </td>
                                    </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <a href="index.php"><b>Go to the list </b></a>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../resource/js/bootstrap.min.js"></script>
    </body>
</html>