<?php
session_start();
include_once("../../"."vendor/autoload.php");

use \App\Mini_Project\PhoneBook_class\Login;
use \App\Mini_Project\PhoneBook_class\Phone_book;
use \App\Mini_Project\Utility\Utility;

$obj = new Phone_book();
$view = $obj->show($_GET['id']);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Phone Book Information List</title>
        <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        h2{
            color : rosybrown;
        }
        button .btn-info{
            color : white;
        }
    </style>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h2>Phone Book View</h2> 
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td><?php echo $view->name; ?></td>
                                    <td><?php echo $view->email; ?></td>
                                    <td><?php echo $view->mobile; ?></td>
                                    <td><?php echo $view->address; ?></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <a href="index.php"><b>Go back to Phone Book List</b></a>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../resource/js/bootstrap.min.js"></script>
    </body>
</html>
