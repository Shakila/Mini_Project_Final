<?php
session_start();
include_once("../../"."vendor/autoload.php");

use \App\Mini_Project\PhoneBook_class\Login;
use \App\Mini_Project\PhoneBook_class\Phone_book;
use \App\Mini_Project\Utility\Utility;

$obj = new Phone_book();
$obj->prepare($_REQUEST)->store();
?>
