<?php
session_start();
include_once("../../"."vendor/autoload.php");

use \App\Mini_Project\PhoneBook_class\Login;
use \App\Mini_Project\PhoneBook_class\Phone_book;
use \App\Mini_Project\Utility\Utility;

$obj = new Phone_book();
$thepb = $obj->edit($_GET['id']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Update Information</title>
        <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        h3{
            color : rosybrown;
        }
    </style>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h3>Phone Book Information</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form role="form"  action="update.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $thepb->id;?>" />
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" name="name" value="<?php echo $thepb->name ?>" class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" name="email"  value="<?php echo $thepb->email ?>" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile#</label>
                            <input type="text" name="mobile" pattern="[+]{1}[0-9]{13}"  value="<?php echo $thepb->mobile ?>" class="form-control" id="mobile">
                        </div>
                        <div class="form-group">
                            <label for="address">Address:</label>
                            <textarea name="address" class="form-control" rows="3" id="address"><?php echo $thepb->address ?></textarea>
                        </div>

                        <div class="btn-group">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="submit" class="btn btn-warning"><a href="index.php">List</a></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../resource/js/bootstrap.min.js"></script>
    </body>
</html>